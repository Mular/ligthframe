let pContainer = "#auth-container";

function loadView(view) {
  $.ajax({
    url: 'auth/loadView',
    type: 'POST',
    data: {view: view}
  }).done(function (result){
    //let data = JSON.parse(result);
    $(pContainer).html('');
    $(pContainer).html(result);
  });
}