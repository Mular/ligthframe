<?php

// Libraries
require_once 'config/config.php';

// Helpers
require_once 'helpers/url_helper.php';
require_once 'helpers/get_date_helper.php';
require_once 'helpers/add_helper.php';
require_once 'helpers/dd_helper.php';

// Autoload php carga todas las clases existentes en la carpeta core.
// Las clases se deben llamar exactamente igual que los archivos

spl_autoload_register(function($class){
  require_once 'core/'.$class.'.php';
});
