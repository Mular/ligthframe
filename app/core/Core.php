<?php

#. MAPEAR LA URL INGRESADA EN EL NAVEGADOR
#. 1-Controlador
#. 2-Método
#. 3-Parámetro
#. EJ: /articulos/bebidas/8

class Core {

  protected $controladorActual = 'Auth';
  protected $metodoActual = 'index';
  protected $parametros = [];

  public function __construct(){
    // print_r($this->getUrl());
    $url = $this->getUrl();

    // BUSCA EL CONTROLADOR SI EXISTE
    if (file_exists('../app/controller/'.$url[0].'.php')) {
      // SI EXISTE SE CONFIGURA COMO CONTROLADOR POR DEFECTO
      $this->controladorActual = $url[0];

      unset($url[0]);
    }

    // Requiere el controlador
    require_once '../app/controller/'.$this->controladorActual.'.php';
    $this->controladorActual = new $this->controladorActual;

    // Verifica la segunda parte de la url (método)
    if (isset($url[1])) {
      if (method_exists($this->controladorActual, $url[1])) {
        $this->metodoActual = $url[1];

        unset($url[1]);
      }
    }

    // Para probar que si trae el método
    // echo $this->metodoActual;

    // Verifica el valor o parametro (tercera parte de la url)
    $this->parametros = $url ? array_values($url) : [];

    //Llamar callback con parametros array
    call_user_func_array([$this->controladorActual, $this->metodoActual], $this->parametros);
  }

  public function getUrl(){
    // echo $_GET['url'];

    if (isset($_GET['url'])) {
      $url = rtrim($_GET['url'], '/');
      $url = filter_var($url, FILTER_SANITIZE_URL);
      return explode('/', $url);
    }else {
      header('Location: '. $this->controladorActual);
    }

  }
}
