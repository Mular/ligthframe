<?php

/**
 * CONEXION A LA BASE DE DATOS Y REALIZAR QUERY'S
 */
class DataBase {

  private $host = DB_HOST;
  private $user = DB_USER;
  private $pass = DB_PASS;
  private $port = DB_PORT;
  private $db = DB_NAME;
  private $charset = DB_CHARSET;

  private $dbh; // Database handled
  private $stm;
  private $err;

  public function __construct(){
    $dsn = 'mysql:host='.$this->host .';port='.$this->port .';dbname='.$this->db;

    $options = array(
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );

    try {

      $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
      $this->dbh->exec('set names '.$this->charset);

      // Para pruebas de conexion
      // echo "Si se conecto";

    } catch (PDOException $e) {
      $this->err = $e->getMessage();
      die($this->err);
    }

  }

  // Prepara la consulta
  public function Query($sql){
    $this->stm = $this->dbh->prepare($sql);
  }

  // Se vincula la consulta con bind (obtener)
  public function bind($param, $value, $type = null){
    if (is_null($type)) {
      switch (true) {
        case is_int($value):
          $type = PDO::PARAM_INT;
          break;
        case is_bool($value):
          $type = PDO::PARAM_BOOL;
          break;
        case is_null($value):
          $type = PDO::PARAM_NULL;
          break;

        default:
          $type = PDO::PARAM_STR;
          break;
      }
    }

    $this->stm->bindValue($param, $value, $type);

  }

  //Ejecuta la consulta
  public function Execute(){
    //Se ejecuta solo porque la variable stm esta cargada con el query en el metodo Query
    return $this->stm->execute();
  }

  //Obtener todos los registros de la consulta
  public function getAllRows(){
    $this->Execute();
    return $this->stm->fetchAll(PDO::FETCH_OBJ);
  }

  //Obtener un solo registro de la consulta
  public function getOneRow(){
    $this->Execute();
    return $this->stm->fetch(PDO::FETCH_OBJ);
  }

  //Contar la cantidad de filas que se obtienen del registro
  public function rowCount(){
    $this->Execute();
    return $this->stm->fetchColumn();
  }

}
