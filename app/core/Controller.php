<?php

/**
 * controlador principal
 * Se encarga de cargar los modelos y las vistas
 */
class Controller {

  public function Modelo($modelo){
    require_once '../app/model/'. $modelo .'.php';
    return new $modelo();
  }

  public function Email($mail){
    require_once '../app/controller/'.$mail.'.php';
    return new $mail();
  }

  public function Vista($vista, $param = []){

    if (file_exists('../app/view/'. $vista. '.php')) {
      require_once '../app/view/'. $vista. '.php';
    }else {
      die('El archivo '.$vista.'.php no se ha encontrado.');
    }
  }

  public function resizeImg($original_image_data, $original_width, $original_height, $new_width, $new_height) {

    $dst_img = imagecreatetruecolor($new_width, $new_height);
    imagecolortransparent($dst_img, imagecolorallocate($dst_img, 0, 0, 0));
    imagecopyresampled($dst_img, $original_image_data, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);

    return $dst_img;
  }

}
