<?php

class auth extends Controller {
  private $new_inicio;

	function __construct() {
    $this->new_inicio = $this->Modelo('auth_Model');
    $_SESSION['title'] = 'Inicio';
  }

  public function index() {
    $this->Vista('layout/header');
    $this->Vista('auth/index');
    $this->Vista('layout/footer');
  }

  public function loadView() {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      
      //dd('../app/view/'.$_POST['view'].'.php');
      
      echo file_get_contents('../app/view/'.$_POST['view'].'.php');
      
    }else {
      redirect();
    }
  }

}
