<?php

// Helper para obtener la fecha de hoy
function get_date(){
  date_default_timezone_set('America/Bogota');

  $localtime_assoc = localtime(time(), true);

  // Poner cero al mes
  if (($localtime_assoc['tm_mon'] + 1) < 10) {
    $mon = '0'.($localtime_assoc['tm_mon'] + 1);
  }else {
    $mon = ($localtime_assoc['tm_mon'] + 1);
  }

  // Poner cero al dia
  if ($localtime_assoc['tm_mday'] < 10) {
    $day = '0'.$localtime_assoc['tm_mday'];
  }else {
    $day = $localtime_assoc['tm_mday'];
  }

  // Poner cero al minuto
  if ($localtime_assoc['tm_min'] < 10) {
    $min = '0'.$localtime_assoc['tm_min'];
  }else {
    $min = $localtime_assoc['tm_min'];
  }

  // Poner cero al segundo
  if (($localtime_assoc['tm_sec'] + 1) < 10) {
    $sec = '0'.($localtime_assoc['tm_sec'] + 1);
  }else {
    $sec = ($localtime_assoc['tm_sec'] + 1);
  }

  // Mejorar la vista del segundo
  if ($sec == '60') {
    $sec = '00';
  }

  $fecha = ($localtime_assoc['tm_year'] + 1900) .'-'. $mon .'-'. $day .' '. $localtime_assoc['tm_hour'] .':'. $min .':'. $sec;

  return $fecha;
}
